#!/bin/sh
rm -rf /var/lib/dkms/sdhci-pci/
set -e
dkms install -m sdhci-pci -v 0.1
set +e
modprobe sdhci
modprobe sdhci-pci
